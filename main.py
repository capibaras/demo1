import tkinter as tk
from PIL import Image, ImageTk
import cv2
import os
from recognition import FaceRecognition
from recognition import face_confidence

raiz=tk.Tk()
raiz.title("Identificación de usuarios")
raiz.geometry("650x350")
raiz.resizable(False, False)
raiz.config(bg="DarkOliveGreen3")
video = None


def ventana_register():
    ventana = tk.Toplevel(raiz)
    ventana.title("Register")
    ventana.config(bg="SteelBlue2")
    ventana.resizable(True, True)
    ventana.geometry("1080x720")

    #Campo nombre y apellido
    etiq_nombre = tk.Label(ventana, text="Nombre y apellido:", bg="SteelBlue2")
    etiq_nombre.place(x="10", y="50")
    nombre_entry = tk.Entry(ventana, width=30)
    nombre_entry.place(x=155, y=50)
    nombre_entry.config(width=30)


    def comparar():
        archivos = os.listdir("./faces")
        diferenciar = nombre_entry.get() + ".png"
        for archivo in archivos:
            if archivo == diferenciar:
                mensaje_error = tk.Label(ventana, text= nombre_entry.get() + " ya es un usuario registrado")
                mensaje_error.place(x="650", y="525")
                mensaje_error.config(bg="white", fg="red", font=("Serif", 10, "bold"))
                def eliminar_mensaje():
                    mensaje_error.destroy()                
                ventana.after(5000, eliminar_mensaje)
                return
                        
        def capturar_imagen():
            global imagen_capturada
            captura = cv2.VideoCapture(0)
            ret, imagen = captura.read()
            captura.release()
            imagen_capturada = cv2.cvtColor(imagen, cv2.COLOR_BGR2RGB)
            ruta_guardado = "./faces/"
            nombre_archivo = nombre_entry.get() + ".png"
            ruta_completa = os.path.join(ruta_guardado, nombre_archivo)
            cv2.imwrite(ruta_completa, imagen)
            mostrar_imagen(ruta_completa)

    
        def mostrar_imagen(ruta_completa):
            image = Image.open(ruta_completa)
            tk_image = ImageTk.PhotoImage(image)
            etiqueta_imagen = tk.Label(ventana, image=tk_image)
            etiqueta_imagen.configure(width=640, height=480)
            etiqueta_imagen.image = tk_image
            etiqueta_imagen.place(x=450, y=15)
            etiq_usuario = tk.Label(ventana, text= nombre_entry.get() + " ha sido registrado correctamente !")
            etiq_usuario.place(x="650", y="525")
            etiq_usuario.config(bg="white", fg="green", font=("Serif", 10, "bold"))
    
            def eliminar_usuario():
                etiq_usuario.destroy()                
            ventana.after(5000, eliminar_usuario)
        capturar_imagen()
        mostrar_imagen()
    #Boton añadir
    boton_ventana_añadir = tk.Button(ventana, text="Añadir usuario", command=comparar)
    boton_ventana_añadir.place(x="155", y="200")
    boton_ventana_añadir.config(cursor="hand2", bg="grey", relief="flat", width=10, height=1, font=("Calisto MT", 12, "bold"))


    ventana.mainloop()


def video_reconocimiento():
    if __name__ == '__main__':
        fr = FaceRecognition()
        fr.run_recognition()



#Botones

boton_registrarse = tk.Button(raiz, text="REGISTRARSE", command=ventana_register, width=15, height=1)
boton_registrarse.place(x="250", y="175")
boton_registrarse.config(cursor="hand2", bg="gray62", font=("Serif", 12, "bold") )

boton_video = tk.Button(raiz, text="VIDEO", command=video_reconocimiento, width=15, height=1)
boton_video.place(x="250", y="125")
boton_video.config(cursor="hand2", bg="gray62", font=("Serif", 12, "bold"))
   

raiz.mainloop()
